import 'package:flutter/material.dart';
import 'package:shopifyadmin/screens/admin.dart';

void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Admin(),
  ));
}