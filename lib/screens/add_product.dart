import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';

import 'package:flutter/material.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:shopifyadmin/db/category.dart';
import 'package:shopifyadmin/db/brand.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shopifyadmin/db/product.dart';

class AddProduct extends StatefulWidget {
  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  Color white = Colors.white;
  Color black = Colors.black;
  Color grey = Colors.grey;
  Color green = Colors.green;

  CategoryService _categoryService = CategoryService();
  BrandService _brandService = BrandService();
  ProductService _productService = ProductService();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController productNameController = TextEditingController();
  TextEditingController quantityController = TextEditingController();
  final TextEditingController priceController = TextEditingController();
  List<DocumentSnapshot> brands = <DocumentSnapshot>[];
  List<DocumentSnapshot> categories = <DocumentSnapshot>[];
  List<DropdownMenuItem<String>> categoriesDropdown = [];
  List<DropdownMenuItem<String>> brandDropdown = [];
  String _currentCategory;
  String _currentBrand;

  List<String> selectedSizes = <String>[];
  File _image1;
  File _image2;
  File _image3;
  bool isLoading = false;

  @override
  void initState() {
    _getCategories();
    _getBrands();
//    FutureBuilder builder =  _getCategories();
//    _getBrands();
//    categoriesDropdown = getCategoriesDropdown();
//    print(categories);
//    _currentCategory = categoriesDropdown[0].value;
    super.initState();
  }

  List<DropdownMenuItem<String>> getCategoriesDropdown(){
    List<DropdownMenuItem<String>> items = List();

    if(categories.length > categoriesDropdown.length)
      for(DocumentSnapshot category in categories){
        items.add(DropdownMenuItem(child: Text(category.data['category']), value: category.data['category']));
      }
    else
      return categoriesDropdown;
    return items;
  }
  List<DropdownMenuItem<String>> getBrandsDropdown(){
    List<DropdownMenuItem<String>> items = List();
    print(brands.length);
    print(brandDropdown.length);
    if(brands.length > brandDropdown.length)
      for(DocumentSnapshot brand in brands){
        items.add(DropdownMenuItem(child: Text(brand.data['brand']), value: brand.data['brand']));
      }
    else
      return brandDropdown;
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        backgroundColor: white,
        leading: Icon(Icons.close, color: black),
        title: Text('Add product', style: TextStyle(color: black)),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: isLoading ? Center(child:CircularProgressIndicator()): Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: OutlineButton(
                      borderSide: BorderSide(color: grey.withOpacity(0.5), width: 2.0),
                      onPressed: (){
                        _selectImage(ImagePicker.pickImage(source: ImageSource.gallery), 1);
                      },
                      child: _displayChild1(),

                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: OutlineButton(
                      borderSide: BorderSide(color: grey.withOpacity(0.5), width: 2.0),
                      onPressed: (){
                        _selectImage(ImagePicker.pickImage(source: ImageSource.gallery), 2);
                      },
                      child: _displayChild2(),

                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: OutlineButton(
                      borderSide: BorderSide(color: grey.withOpacity(0.5), width: 2.0),
                      onPressed: (){
                        _selectImage(ImagePicker.pickImage(source: ImageSource.gallery), 3);
                      },
                      child: _displayChild3(),

                    ),
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Enter a product name with 10 letters max',
                textAlign: TextAlign.center,
                style: TextStyle(color: green, fontSize: 12),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: TextFormField(
                controller: productNameController,
                decoration: InputDecoration(
                  hintText: 'Product name',
//                  border: InputBorder.none
                ),
                validator: (value){
                  if (value.isEmpty){
                    return 'You must enter the product name';
                  } else if(value.length > 10){ // shitty code
                    return 'Product name  cant be more than 10 letters';
                  }
                  return null;
                },
              ),
            ),
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Category: ", style: TextStyle(color: green),),
                ),
                DropdownButton(
                    items: categoriesDropdown,
                    onChanged: changeSelectedCategory,
                    value: _currentCategory,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Brand: ", style: TextStyle(color: green),),
                ),
                DropdownButton(
                  items: brandDropdown,
                  onChanged: changeSelectedBrand,
                  value: _currentBrand,
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: TextFormField(
                controller: quantityController,

                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'Quantity',
                  hintText: '1',
//                  border: InputBorder.none
                ),
                validator: (value){
                  if (value.isEmpty){
                    return 'You must enter value';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: TextFormField(

                controller: priceController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'Price',
                  hintText: '0.00',
//                  border: InputBorder.none
                ),
                validator: (value){
                  if (value.isEmpty){
                    return 'You must enter the price';
                  }
                  return null;
                },
              ),
            ),
            Row(
              children: <Widget>[
                Checkbox(value: selectedSizes.contains('XS'), onChanged: (value) => changeSelectedSize('XS')),
                Text('XS'),
                Checkbox(value: selectedSizes.contains('S'), onChanged: (value) => changeSelectedSize('S')),
                Text('S'),
                Checkbox(value: selectedSizes.contains('M'), onChanged: (value) => changeSelectedSize('M')),
                Text('M'),
                Checkbox(value: selectedSizes.contains('L'), onChanged: (value) => changeSelectedSize('L')),
                Text('L'),
                Checkbox(value: selectedSizes.contains('XL'), onChanged: (value) => changeSelectedSize('XL')),
                Text('XL'),
                Checkbox(value: selectedSizes.contains('XXL'), onChanged: (value) => changeSelectedSize('XXL')),
                Text('XXL'),
              ],
            ),
            Row(
              children: <Widget>[
                Checkbox(value: selectedSizes.contains('28'), onChanged: (value) => changeSelectedSize('28')),
                Text('28'),
                Checkbox(value: selectedSizes.contains('30'), onChanged: (value) => changeSelectedSize('30')),
                Text('30'),
                Checkbox(value: selectedSizes.contains('32'), onChanged: (value) => changeSelectedSize('32')),
                Text('32'),
                Checkbox(value: selectedSizes.contains('34'), onChanged: (value) => changeSelectedSize('34')),
                Text('34'),

                Checkbox(value: selectedSizes.contains('36'), onChanged: (value) => changeSelectedSize('36')),
                Text('36'),
                Checkbox(value: selectedSizes.contains('38'), onChanged: (value) => changeSelectedSize('38')),
                Text('38'),

              ],
            ),
            Row(
              children: <Widget>[
                Checkbox(value: selectedSizes.contains('40'), onChanged: (value) => changeSelectedSize('40')),
                Text('40'),
                Checkbox(value: selectedSizes.contains('42'), onChanged: (value) => changeSelectedSize('42')),
                Text('42'),
                Checkbox(value: selectedSizes.contains('44'), onChanged: (value) => changeSelectedSize('44')),
                Text('44'),
                Checkbox(value: selectedSizes.contains('46'), onChanged: (value) => changeSelectedSize('46')),
                Text('46'),

                Checkbox(value: selectedSizes.contains('48'), onChanged: (value) => changeSelectedSize('48')),
                Text('48'),
                Checkbox(value: selectedSizes.contains('50'), onChanged: (value) => changeSelectedSize('50')),
                Text('50'),

              ],
            ),

            FlatButton(
              color: green,
              textColor: white,
              child: Text('Add product'),
              onPressed: (){
                validateAndUpload();
              },
            )

          ],
        ),
    )
      ),
    );
  }

  _getCategories() async{
    List<DocumentSnapshot> data = await _categoryService.getCategories();
    setState((){
      categories = data;
      categoriesDropdown = getCategoriesDropdown();
      _currentCategory = categories[0].data['category'];
    });


  }
  _getBrands() async {
    List<DocumentSnapshot> data = await _brandService.getBrands();
    setState(() {
      brands = data;
      brandDropdown = getBrandsDropdown();
      _currentBrand = brands[0].data['brand'];
    });
  }
  changeSelectedCategory(String selectedCategory){
    setState(() => _currentCategory = selectedCategory);
  }
  changeSelectedBrand(String selectedCategory){
    setState(() => _currentCategory = selectedCategory);
  }



  changeSelectedSize(String size) {
    if(selectedSizes.contains(size)){
      setState(() {
        selectedSizes.remove(size);
      });
    }else{
      setState(() {
        selectedSizes.add(size);
      });

    }
  }

  void _selectImage(Future<File> pickImage, int imageNumber) async{
    File tempImg = await pickImage;
    switch(imageNumber){
      case 1: setState(() {
        _image1 = tempImg;
      });
      break;
      case 2: setState(() {
        _image2 = tempImg;
      });
      break;
      case 3: setState(() {
        _image3 = tempImg;
      });
      break;
    }

  }

  Widget _displayChild1() {
    if(_image1 == null){
      return Padding(
          padding: const EdgeInsets.fromLTRB(8.0, 40.0, 8.0, 40.0),
          child: Icon(Icons.add, color: grey,));
    }else{
      return Image.file(_image1, fit: BoxFit.fill, width: double.infinity,);
    }
  }

  Widget _displayChild2() {
    if(_image2 == null){
      return Padding(
          padding: const EdgeInsets.fromLTRB(8.0, 40.0, 8.0, 40.0),
          child: Icon(Icons.add, color: grey,));
    }else{
      return Image.file(_image2, fit: BoxFit.fill, width: double.infinity,);
    }
  }

  Widget _displayChild3() {
    if(_image3 == null){
      return Padding(
          padding: const EdgeInsets.fromLTRB(8.0, 40.0, 8.0, 40.0),
          child: Icon(Icons.add, color: grey,));
    }else{
      return Image.file(_image3, fit: BoxFit.fill, width: double.infinity,);
    }
  }

  void validateAndUpload() async{
    if(_formKey.currentState.validate()){
      setState(()=>isLoading=true);
      if(_image1 != null && _image2 != null && _image3 != null){
        if(selectedSizes.isNotEmpty){
          final FirebaseStorage storage = FirebaseStorage.instance;
          String imageUrl1;
          String imageUrl2;
          String imageUrl3;

          final String picture1 = '1${DateTime.now().millisecondsSinceEpoch.toString()}.jpg';
          StorageUploadTask task1 = storage.ref().child(picture1).putFile(_image1);

          final String picture2 = '2${DateTime.now().millisecondsSinceEpoch.toString()}.jpg';
          StorageUploadTask task2 = storage.ref().child(picture2).putFile(_image2);

          final String picture3 = '3${DateTime.now().millisecondsSinceEpoch.toString()}.jpg';
          StorageUploadTask task3 = storage.ref().child(picture3).putFile(_image3);

          StorageTaskSnapshot snapshot1 = await task1.onComplete.then((value) => value);
          StorageTaskSnapshot snapshot2 = await task2.onComplete.then((value) => value);
          task3.onComplete.then((snapshot3) async{
            imageUrl1 = await snapshot1.ref.getDownloadURL();
            imageUrl2 = await snapshot2.ref.getDownloadURL();
            imageUrl3 = await snapshot3.ref.getDownloadURL();
            List<String> imageList = [imageUrl1, imageUrl2, imageUrl3];
            
            _productService.uploadProduct(
              productName: productNameController.text,
              price: double.parse(priceController.text),
              brand: _currentBrand,
              category: _currentCategory,
              sizes: selectedSizes,
              images: imageList,
              quantity: int.parse(quantityController.text)
            );
            _formKey.currentState.reset();

            setState(() => isLoading = false);
            Fluttertoast.showToast(msg: 'Product added');
            Navigator.pop(context);
          });

        }else{
          setState(() => isLoading = false);
          Fluttertoast.showToast(msg: 'Select atleast one size');
        }
      }else{
        setState(() => isLoading = false);
        Fluttertoast.showToast(msg: 'All images must be provided');
      }
    }
  }
}

